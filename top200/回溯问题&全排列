回溯问题&全排列

题目：
给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。

题解：

  解题思路：
    选择数组的首元素为初始点，递归向下选择剩余的元素中的一个作为下个排列位置的元素,一直递归,直到
	链表的长度和数组的长度相等表示本次排列完毕,添加到集合中,然后回退到上一个位置,去掉末尾元素,
	因为当前剩余元素还是1,所以继续回退一步,并将其标注为未读取,从新进行链表排序,就这样就可以将所有的
	元素全部排列一遍
	
题解代码：
class Solution {
    public List<List<Integer>> permute(int[] nums) {
        int len = nums.length;
        // 使用一个动态数组保存所有可能的全排列
        List<List<Integer>> res = new ArrayList<>();
        if (len == 0) {
            return res;
        }

		//用于标注当前元素的状态
        boolean[] used = new boolean[len];
        Deque<Integer> path = new ArrayDeque<>(len);

        dfs(nums, len, 0, path, used, res);
        return res;
    }

    private void dfs(int[] nums, int len, int depth,
                     Deque<Integer> path, boolean[] used,
                     List<List<Integer>> res) {
		//递归结束条件
        if (depth == len) {
            res.add(new ArrayList<>(path));
            return;
        }

        for (int i = 0; i < len; i++) {
			//判断当前元素的状态,查看是否已标记
            if (!used[i]) {
                path.addLast(nums[i]);
                used[i] = true;

                dfs(nums, len, depth + 1, path, used, res);

				//设置为未读取状态
                used[i] = false;
				//从队列中剔除该元素
                path.removeLast();
            }
        }
    }
}